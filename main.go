package main

import (
	"apiDigimon/resources"
	"fmt"
	"github.com/labstack/echo"
	"log"
)

func main() {
	fmt.Println("Welcome to The Digimon API")

	e := echo.New()

	e.GET("/digimons", resources.GetDigimons)
	e.GET("/digimon/:name", resources.GetDigimon)
	e.GET("/digimon/:name/techniques", resources.GetTechniques)
	e.GET("/digimon/:name/evolution/to", resources.GetEvolutions)
	e.GET("/digimon/:name/evolution/from", resources.GetDevolutions)

	e.POST("/digimons", resources.AddDigimon)
	e.POST("/digimon/:name/techniques", resources.AddTechnique)
	e.POST("/digimon/:name/evolution/to", resources.AddEvolution)
	e.POST("/digimon/:name/evolution/from", resources.AddDevolution)

	e.DELETE("/digimon/:name", resources.RemDigimon)
	e.DELETE("/digimon/:name/techniques/:tech_name", resources.RemTechnique)
	e.DELETE("/digimon/:name/evolution/to/:evo_name", resources.RemEvolution)
	e.DELETE("/digimon/:name/evolution/from/:evo_name", resources.RemDevolution)

	err := e.Start(":8000")
	log.Fatal(err)
}
