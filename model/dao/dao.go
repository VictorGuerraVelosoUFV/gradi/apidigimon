package dao

import (
	"apiDigimon/config"
	"apiDigimon/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

var cfg = config.Config{}
var Dao = DigimonDAO{}

func init() {
	cfg.Read()

	Dao.Connect(cfg.Server, cfg.Database)
}

type DigimonDAO struct {
	Database   *mgo.Database
	Collection *mgo.Collection
}

func (d *DigimonDAO) Connect(server string, database string) {
	session, err := mgo.Dial(server)
	if err != nil {
		log.Fatal(err)
	}
	d.Database = session.DB(database)
	d.Collection = d.Database.C("digimon")
}

func (d DigimonDAO) GetAll() ([]model.Digimon, error) {
	var digimons []model.Digimon
	err := d.Collection.Find(bson.M{}).All(&digimons)
	return digimons, err
}

func (d DigimonDAO) GetByName(name string) (model.Digimon, error) {
	var digimon model.Digimon
	err := d.Collection.Find(bson.M{"name": name}).One(&digimon)
	return digimon, err
}

func (d DigimonDAO) GetByID(id bson.ObjectId) (model.Digimon, error) {
	var digimon model.Digimon
	err := d.Collection.FindId(id).One(&digimon)
	return digimon, err
}

func (d DigimonDAO) Create(digimon *model.Digimon) error {
	err := d.Collection.Insert(digimon)
	return err
}

func (d DigimonDAO) DeleteByID(id bson.ObjectId) error {
	err := d.Collection.RemoveId(id)
	return err
}

func (d DigimonDAO) DeleteByName(name string) error {
	err := d.Collection.Remove(bson.M{"name": name})
	return err
}

func (d DigimonDAO) Update(id bson.ObjectId, digimon model.Digimon) error {
	err := d.Collection.UpdateId(id, &digimon)
	return err
}
