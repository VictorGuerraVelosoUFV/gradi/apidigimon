package model

import "gopkg.in/mgo.v2/bson"

type Digimon struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id" xml:"id"`
	Name        string        `bson:"name" json:"name" xml:"name"`
	Description string        `bson:"description" json:"description" xml:"description"`
	Image       string        `bson:"image" json:"image" xml:"image"`
	Type        []string      `bson:"type" json:"type" xml:"type"`
	Level       []string      `bson:"level" json:"level" xml:"level"`
	Attribute   []string      `bson:"attribute" json:"attribute" xml:"attribute"`
	Fields      []string      `bson:"fields" json:"fields" xml:"fields"`
	Weight      string        `bson:"weight" json:"weight" xml:"weight"`
	Techniques  []Technique   `bson:"techniques" json:"techniques" xml:"techniques"`
	Evolutions  []string      `bson:"evolutions" json:"evolutions" xml:"evolutions"`
	Devolutions []string      `bson:"devolutions" json:"devolutions" xml:"devolutions"`
}

type Technique struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id" xml:"id"`
	Name        string        `bson:"name" json:"name" xml:"name"`
	Description string        `bson:"description" json:"description" xml:"description"`
}
