package resources

import (
	"apiDigimon/model"
	"apiDigimon/model/dao"
	"fmt"
	"github.com/labstack/echo"
	"net/http"
)

func GetDigimons(c echo.Context) error {
	digimons, err := dao.Dao.GetAll()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimons)
}

func GetDigimon(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon)
}

func AddDigimon(c echo.Context) error {
	d := new(model.Digimon)
	if err := c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	if err := dao.Dao.Create(d); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, d)
}

func RemDigimon(c echo.Context) error {
	name := c.Param("name")
	if err := dao.Dao.DeleteByName(name); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, fmt.Sprintf("Successfully removed %s", name))
}

func AddEvolution(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	d := new(string)
	if err = c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	digimon.Evolutions = append(digimon.Evolutions, *d)
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Evolutions)
}

func GetEvolutions(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Evolutions)
}

func RemEvolution(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	evol := digimon.Evolutions
	for i, v := range evol {
		if v == c.Param("evo_name") {
			evol = append(evol[:i], evol[i+1:]...)
			break
		}
	}
	digimon.Evolutions = evol
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Evolutions)
}

func AddDevolution(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	d := new(string)
	if err = c.Bind(d); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	digimon.Devolutions = append(digimon.Devolutions, *d)
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Devolutions)
}

func GetDevolutions(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Devolutions)
}

func RemDevolution(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	devol := digimon.Devolutions
	for i, v := range devol {
		if v == c.Param("evo_name") {
			devol = append(devol[:i], devol[i+1:]...)
			break
		}
	}
	digimon.Devolutions = devol
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Devolutions)
}

func AddTechnique(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	t := new(model.Technique)
	if err = c.Bind(t); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	digimon.Techniques = append(digimon.Techniques, *t)
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Techniques)
}

func GetTechniques(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Techniques)
}

func RemTechnique(c echo.Context) error {
	digimon, err := dao.Dao.GetByName(c.Param("name"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	tech := digimon.Techniques
	for i, v := range tech {
		if v.Name == c.Param("tech_name") {
			tech = append(tech[:i], tech[i+1:]...)
			break
		}
	}
	digimon.Techniques = tech
	err = dao.Dao.Update(digimon.ID, digimon)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, digimon.Techniques)
}
